<?php

namespace App\Repository;

use App\Entity\InfosCommune;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InfosCommune|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfosCommune|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfosCommune[]    findAll()
 * @method InfosCommune[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfosCommuneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfosCommune::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('i')
            ->where('i.something = :value')->setParameter('value', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
