<?php

namespace App\Controller;

use App\Entity\Commune;
use App\Entity\Document;
use App\Entity\InfosCommune;
use App\Entity\ParametreQualite;
use Psr\Log\LoggerInterface;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Classe exposant les appels d'API pour EDMC (version 2016-2017)
 *
 * @Route("/api/v1/edmc")
 */
class EDMCController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoggerInterface $logger,
        SerializerInterface $serializer
    ) {
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * Retourne l'ensemble des informations connues pour le code INSEE
     *
     * @param Request $request
     * @param string $code_insee Code INSEE de la commune
     *
     * @return Response
     *
     * @SWG\Response(
     *      response="200",
     *      description="Cas nominal",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="code", type="string", description="Status code, 200 if OK"),
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Property(property="calcaire", type="string", description="Indice en °F"),
     *              @SWG\Property(property="prix", type="string", description="Prix du m3 d'eau"),
     *              @SWG\Property(property="qualite", type="array",
     *                  @SWG\Items(
     *                       type="object",
     *                       @SWG\Property(property="nom", type="string", description="Nom du paramètre"),
     *                       @SWG\Property(property="valeur", type="string", description="Valeur du paramètre"),
     *                       @SWG\Property(property="unite", type="string", description="Unité"),
     *                  ),
     *              ),
     *              @SWG\Property(property="documents", type="array",
     *                  @SWG\Items(
     *                       type="object",
     *                       @SWG\Property(property="nom", type="string", description="Nom du document (affiché à l'internaute)"),
     *                       @SWG\Property(property="url", type="string", description="URL d'accès au document, exemple http(s)://domain.tld/chemin/vers/document.ext"),
     *                       @SWG\Property(property="taille", type="string", description="Taille du document en octets"),
     *                       @SWG\Property(property="type", type="string", description="Type MIME, exemple 'application/pdf'"),
     *                  ),
     *              ),
     *          ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC")
     *
     * @Route("/{code_insee}/infos", name="edmc_infos", methods={"GET"})
     */
    public function infosAction(Request $request, $code_insee)
    {

        $result = $this->getDataForCodeInsee($code_insee, true, true, true, true);
        $response = new Response(
            $this->serializer->serialize($result, 'json', ['groups' => ['saur']]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * Retourne le prix de l'eau pour le code INSEE
     *
     * @param string $code_insee Code INSEE de la commune
     *
     * @return Response
     *
     * @SWG\Response(
     *      response="200",
     *      description="Cas nominal",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="code", type="string", description="Status code, 200 if OK"),
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Property(property="prix", type="string", description="Prix du m3 d'eau"),
     *          ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC")
     *
     * @Route("/{code_insee}/prix", name="edmc_prix", methods={"GET"})
     */
    public function prixAction(Request $request, $code_insee)
    {

        $result = $this->getDataForCodeInsee($code_insee, true, false, false, false);
        $response = new Response(
            $this->serializer->serialize($result, 'json', ['groups' => ['saur']]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * Retourne l'info calcaire pour le code INSEE
     *
     * @param string $code_insee Code INSEE de la commune
     *
     * @return Response
     *
     * @SWG\Response(
     *      response="200",
     *      description="Cas nominal",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="code", type="string", description="Status code, 200 if OK"),
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Property(property="calcaire", type="string", description="Indice en °F"),
     *          ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC")
     *
     * @Route("/{code_insee}/calcaire", name="edmc_calcaire", methods={"GET"})
     */
    public function calcaireAction(Request $request, $code_insee)
    {

        $result = $this->getDataForCodeInsee($code_insee, false, true, false, false);
        $response = new Response(
            $this->serializer->serialize($result, 'json', ['groups' => ['saur']]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * Retourne les paramètres qualité pour le code INSEE
     *
     * @param string $code_insee Code INSEE de la commune
     *
     * @return Response
     *
     * @SWG\Response(
     *      response="200",
     *      description="Cas nominal",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="code", type="string", description="Status code, 200 if OK"),
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Property(property="qualite", type="array",
     *                  @SWG\Items(
     *                       type="object",
     *                       @SWG\Property(property="nom", type="string", description="Nom du paramètre"),
     *                       @SWG\Property(property="valeur", type="string", description="Valeur du paramètre"),
     *                       @SWG\Property(property="unite", type="string", description="Unité"),
     *                  ),
     *              ),
     *          ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC")
     *
     * @Route("/{code_insee}/qualite", name="edmc_qualite", methods={"GET"})
     */
    public function qualiteAction(Request $request, $code_insee)
    {

        $result = $this->getDataForCodeInsee($code_insee, false, false, true, false);
        $response = new Response(
            $this->serializer->serialize($result, 'json', ['groups' => ['saur']]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * Retourne les documents liés à la ville dont le code INSEE est passé en paramètre
     *
     * @param string $code_insee Code INSEE de la commune
     *
     * @return Response
     *
     * @SWG\Response(
     *      response="200",
     *      description="Cas de figure normal qu'il y ait des documents ou non pour ce code INSEE",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="code", type="string", description="Status code, 200 if OK"),
     *              @SWG\Property(property="data", type="string"),
     *          ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC")
     *
     * @Route("/{code_insee}/documents", name="edmc_documents", methods={"GET"})
     */
    public function documentsAction(Request $request, $code_insee)
    {

        $result = $this->getDataForCodeInsee($code_insee, false, false, false, true);
        $response = new Response(
            $this->serializer->serialize($result, 'json', ['groups' => ['saur']]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    private function getDataForCodeInsee($code_insee, $get_prix, $get_calcaire, $get_qualite, $get_documents)
    {
        $this->logger->info('EDMC infos');
        $this->logger->info('Code INSEE '.$code_insee);

        $commune = $this->getDoctrine()
            ->getRepository(Commune::class)
            ->findOneBy(['code_insee' => $code_insee]);

        if ($commune == null) {
            $result['code'] = '404';
            $result['message'] = 'Commune non trouvée';
        } else {
            $result['code'] = '200';
            $data = null;

            /** @var InfosCommune $infos */
            $infos = $this->getDoctrine()
                ->getRepository(InfosCommune::class)
                ->findOneBy(['commune' => $commune]);

            if ($get_calcaire || $get_prix) {
                $this->logger->info(print_r($infos, true));
                if ($infos != null) {
                    if ($get_prix) {
                        $data['prix'] = $infos->getPrix();
                    }
                    if ($get_calcaire) {
                        $data['calcaire'] = $infos->getCalcaire();
                    }
                }
            }

            if ($get_qualite) {
                $qualite = $this->getDoctrine()
                    ->getRepository(ParametreQualite::class)
                    ->findBy(['commune' => $commune]);
                $data['qualite'] = $qualite;
            }

            if ($get_documents) {
                $documents = $this->getDoctrine()
                    ->getRepository(Document::class)
                    ->findBy(['commune' => $commune]);
                $data['documents'] = $documents;
            }
            $result['data'] = $data;
        }

        return $result;
    }
}
