<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Classe exposant les appels d'API pour EDMC version 2019
 *
 * @Route("/api")
 */
class EDMCNextController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoggerInterface $logger,
        SerializerInterface $serializer
    ) {
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * Retourne les prix des différents produits pour le code INSEE
     *
     * @Route("/prix-eau/{code_insee}", name="edmc_next_prix", methods={"GET"})
     *
     * @param string $code_insee
     *
     * @SWG\Response(
     *      response="200",
     *      description="Réponse en fonctionnement nominal",
     *      @SWG\Schema(
     *          type="object",
     *              @SWG\Property(property="codeInsee", type="string", description="Code INSEE de la ville"),
     *              @SWG\Property(property="produits", type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="refCnp", type="string", description="Référence CNP comptable"),
     *                      @SWG\Property(property="nom", type="string", description="???"),
     *                      @SWG\Property(property="type", type="string", description="Type de produit (quelles valeurs possibles ?)"),
     *                      @SWG\Property(property="sousType", type="string", description="Sous Type si présent (exemple collectifs ou non collectif"),
     *                      @SWG\Property(property="mission", type="string", description="Mission du produit (non utilisé ?)"),
     *                      @SWG\Property(property="prixProduit", type="object",
     *                          @SWG\Property(property="volumeBase", type="integer", description="Volume de base du prix (par défaut 120m3"),
     *                          @SWG\Property(property="montantTtc", type="object",
     *                              @SWG\Property(property="libelle", type="number", description="Prix de l'abonnement du produit (TTC)"),
     *                              @SWG\Property(property="abonnement", type="number", description="Prix de l'abonnement du produit (TTC)"),
     *                              @SWG\Property(property="consommation", type="number", description="Prix de la consommation du produit (TTC)"),
     *                              @SWG\Property(property="totalTtc", type="number", description="Prix total du produit : (Prix de l'abonnement du produit + Prix de la consommation du produit) (TTC)"),
     *                          ),
     *                      ),
     *                  ),
     *              ),
     *              @SWG\Property(property="totaux", type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="libelle", type="string", description="Libellé permettant d'identifier les produits concernés par le total"),
     *                      @SWG\Property(property="abonnement", type="number", description="Prix de l'abonnement total (TTC) (la somme de plusieurs abonnements exemple : prix abonnement eau potable + prix abonnement assainissement collectif)"),
     *                      @SWG\Property(property="consommation", type="number", description="Prix de la consommation total (TTC) (la somme de plusieurs consommations exemple : prix consommation eau potable + prix consommation assainissement collectif)"),
     *                      @SWG\Property(property="totalTtc", type="number", description="Facturation totale (TTC) (Prix de l'abonnement total + Prix de la consommation total)"),
     *                  ),
     *              ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC Next")
     *
     * @return Response
     */
    public function prix(string $code_insee)
    {
        /*$result = [];
        if ('46201' === $code_insee) {
            $result = [
                'codeInsee' => $code_insee,
                'produits' => [
                    [
                        'refCnp' => '465100/01',
                        'nom' => 'SYNDICAT EAU POTABLE ET ASSAINISSEMENT DU QUERCY BLANC',
                        'type' => 'EauPotable',
                        'sousType' => '',
                        'mission' => 'Distribution',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 113.65,
                                'consommation' => 216.93,
                                'totalTtc' => 330.58,
                            ],
                        ],
                    ],
                    [
                        'refCnp' => '465101/02',
                        'nom' => 'SYNDICAT EAU POTABLE ET ASSAINISSEMENT DU QUERCY BLANC',
                        'type' => 'Assainissement',
                        'sousType' => 'Collectif',
                        'mission' => 'Collecte',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 156.32,
                                'consommation' => 267.48,
                                'totalTtc' => 423.8,
                            ],
                        ],
                    ],
                ],
                'totaux' => [
                    'libelle' => 'Eau Potable/Assainissement Collectif',
                    'abonnement' => 269.97,
                    'consommation' => 484.41,
                    'totalTtc' => 754.38,
                ],
            ];
        } elseif ('77132' === $code_insee) {
            $result = [
                'codeInsee' => $code_insee,
                'produits' => [
                    [
                        'refCnp' => '770400/01',
                        'nom' => 'VAL D\'EUROPE AGGLOMERATION',
                        'type' => 'EauPotable',
                        'sousType' => '',
                        'mission' => 'Distribution',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 32.05,
                                'consommation' => 204.29,
                                'totalTtc' => 236.34,
                            ],
                        ],
                    ],
                    [
                        'refCnp' => '770401/02',
                        'nom' => 'VAL D\'EUROPE AGGLOMERATION',
                        'type' => 'Assainissement',
                        'sousType' => 'Collectif',
                        'mission' => 'Collecte',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 11.69,
                                'consommation' => 258.24,
                                'totalTtc' => 269.93,
                            ],
                        ],
                    ],
                ],
                'totaux' => [
                    'libelle' => 'Eau Potable/Assainissement Collectif',
                    'abonnement' => 43.74,
                    'consommation' => 462.53,
                    'totalTtc' => 506.27,
                ],
            ];
        }*/

        $projectDir = $this->getParameter('kernel.project_dir');
        $files = glob($projectDir.'/data/edmc/*-*'.$code_insee.'*Prix.json');
        if ($files && 1 === count($files)) {
            $content = file_get_contents($files[0]);
        } else {
            // Peut être qu'on ne devrait pas retourner HTTP 200 et tableau vide
            // mais je ne sais pas trop comment le client a prévu de gérer ceci
            $content = json_encode([]);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent($content);

        return $response;
    }

    /**
     * Retourne les frais de souscription / résiliation des différents produits pour le code INSEE
     *
     * @Route("/frais/{code_insee}", name="edmc_next_frais", methods={"GET"})
     *
     * @param string $code_insee
     *
     * @SWG\Response(
     *      response="200",
     *      description="Réponse en fonctionnement nominal",
     *      @SWG\Schema(
     *          type="object",
     *              @SWG\Property(property="codeInsee", type="string", description="Code INSEE de la ville"),
     *              @SWG\Property(property="produits", type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="refCnp", type="string", description="Référence CNP comptable"),
     *                      @SWG\Property(property="nom", type="string", description="???"),
     *                      @SWG\Property(property="type", type="string", description="Type de produit (quelles valeurs possibles ?)"),
     *                      @SWG\Property(property="sousType", type="string", description="Sous Type si présent (exemple collectifs ou non collectif"),
     *                      @SWG\Property(property="mission", type="string", description="Mission du produit (non utilisé ?)"),
     *                      @SWG\Property(property="frais", type="array",
     *                          @SWG\Items(type="object",
     *                              @SWG\Property(property="categorie", type="string", description=""),
     *                              @SWG\Property(property="typeService", type="string", description=""),
     *                              @SWG\Property(property="libelle", type="string", description=""),
     *                              @SWG\Property(property="totalTtc", type="number", description=""),
     *                          ),
     *                      ),
     *                  ),
     *              ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC Next")
     *
     * @return Response
     */
    public function frais(string $code_insee)
    {
        /* $result = [];
        if ('46201' === $code_insee) {
            $result = [
                'codeInsee' => $code_insee,
                'produits' => [
                    [
                        'refCnp' => '465100/01',
                        'nom' => 'SYNDICAT EAU POTABLE ET ASSAINISSEMENT DU QUERCY BLANC',
                        'type' => 'EauPotable',
                        'sousType' => '',
                        'mission' => 'Distribution',
                        'frais' => [
                            [
                                'categorie' => 'FraisAdministratif',
                                'typeService' => 'Souscription',
                                'libelle' => 'Autre article',
                                'montantTtc' => 0,
                            ],
                            [
                                'categorie' => 'FraisAdministratif',
                                'typeService' => 'Souscription',
                                'libelle' => 'Frais d\'accès au service',
                                'montantTtc' => 56.85,
                            ],
                            [
                                'categorie' => 'FraisDeDeplacement',
                                'typeService' => 'Resiliation',
                                'libelle' => 'Frais de fermeture pour simple résiliation',
                                'montantTtc' => 56.85,
                            ],
                            [
                                'categorie' => 'FraisDeDeplacement',
                                'typeService' => 'Souscription',
                                'libelle' => 'Frais mise en service de branchement ',
                                'montantTtc' => 28.42,
                            ],
                        ],
                    ],
                ],
            ];
        } elseif ('77132' === $code_insee) {
            $result = [
                'codeInsee' => $code_insee,
                'produits' => [
                    [
                        'refCnp' => '770400/01',
                        'nom' => 'VAL D\'EUROPE AGGLOMERATION',
                        'type' => 'EauPotable',
                        'sousType' => '',
                        'mission' => 'Distribution',
                        'frais' => [
                            [
                                'categorie' => 'FraisAdministratif',
                                'typeService' => 'Souscription',
                                'libelle' => 'Dépôt de garantie',
                                'montantTtc' => 0,
                            ],
                            [
                                'categorie' => 'FraisAdministratif',
                                'typeService' => 'Souscription',
                                'libelle' => 'Frais d\'accès au service',
                                'montantTtc' => 30.31,
                            ],
                            [
                                'categorie' => 'FraisDeDeplacement',
                                'typeService' => 'Resiliation',
                                'libelle' => 'Frais de résiliation avec fermeture',
                                'montantTtc' => 43.62,
                            ],
                            [
                                'categorie' => 'FraisDeDeplacement',
                                'typeService' => 'Souscription',
                                'libelle' => 'Mise en service de branchement à la souscription',
                                'montantTtc' => 31.24,
                            ],
                        ],
                    ],
                    [
                        'refCnp' => '770401/02',
                        'nom' => 'VAL D\'EUROPE AGGLOMERATION',
                        'type' => 'Assainissement',
                        'sousType' => 'Collectif',
                        'mission' => 'Collecte',
                        'frais' => [
                            [
                                'categorie' => 'FraisAdministratif',
                                'typeService' => 'Souscription',
                                'libelle' => 'Frais d\'accès au service',
                                'montantTtc' => 39.27,
                            ],
                        ],
                    ],
                ],
            ];
        }*/

        $projectDir = $this->getParameter('kernel.project_dir');
        $files = glob($projectDir.'/data/edmc/*-*'.$code_insee.'*Frais.json');
        if ($files && 1 === count($files)) {
            $content = file_get_contents($files[0]);
        } else {
            // Peut être qu'on ne devrait pas retourner HTTP 200 et tableau vide
            // mais je ne sais pas trop comment le client a prévu de gérer ceci
            $content = json_encode([]);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent($content);

        return $response;
    }

    /**
     * Retourne les réglements de service pour le code INSEE
     *
     * @Route("/rds/{code_insee}", name="edmc_next_rds", methods={"GET"})
     *
     * @param string $code_insee
     *
     * @SWG\Response(
     *      response="200",
     *      description="Réponse en fonctionnement nominal",
     *      @SWG\Schema(
     *          type="object",
     *              @SWG\Property(property="codeInsee", type="string", description="Code INSEE de la ville"),
     *              @SWG\Property(property="rds", type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="refCnp", type="string", description="Code INSEE de la ville"),
     *                      @SWG\Property(property="type", type="string", description=""),
     *                      @SWG\Property(property="sousType", type="string", description=""),
     *                      @SWG\Property(property="libelle", type="string", description=""),
     *                      @SWG\Property(property="taille", type="integer", description=""),
     *                      @SWG\Property(property="typeMime", type="string", description=""),
     *                      @SWG\Property(property="url", type="string", description=""),
     *                  ),
     *              ),
     *          ),
     *      ),
     *     examples={}
     * )
     * @SWG\Tag(name="EDMC Next")
     *
     * @return Response
     */
    public function rds(string $code_insee)
    {
        /*$result = [];
        if ('46201' === $code_insee) {
            $result = [
                'codeInsee' => $code_insee,
                'produits' => [
                    [
                        'refCnp' => '465100/01',
                        'nom' => 'SYNDICAT EAU POTABLE ET ASSAINISSEMENT DU QUERCY BLANC',
                        'type' => 'EauPotable',
                        'sousType' => '',
                        'mission' => 'Distribution',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 113.65,
                                'consommation' => 216.93,
                                'totalTtc' => 330.58,
                            ],
                        ],
                    ],
                    [
                        'refCnp' => '465101/02',
                        'nom' => 'SYNDICAT EAU POTABLE ET ASSAINISSEMENT DU QUERCY BLANC',
                        'type' => 'Assainissement',
                        'sousType' => 'Collectif',
                        'mission' => 'Collecte',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 156.32,
                                'consommation' => 267.48,
                                'totalTtc' => 423.8,
                            ],
                        ],
                    ],
                ],
                'totaux' => [
                    'libelle' => 'Eau Potable/Assainissement Collectif',
                    'abonnement' => 269.97,
                    'consommation' => 484.41,
                    'totalTtc' => 754.38,
                ],
            ];
        } elseif ('77132' === $code_insee) {
            $result = [
                'codeInsee' => $code_insee,
                'produits' => [
                    [
                        'refCnp' => '770400/01',
                        'nom' => 'VAL D\'EUROPE AGGLOMERATION',
                        'type' => 'EauPotable',
                        'sousType' => '',
                        'mission' => 'Distribution',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 32.05,
                                'consommation' => 204.29,
                                'totalTtc' => 236.34,
                            ],
                        ],
                    ],
                    [
                        'refCnp' => '770401/02',
                        'nom' => 'VAL D\'EUROPE AGGLOMERATION',
                        'type' => 'Assainissement',
                        'sousType' => 'Collectif',
                        'mission' => 'Collecte',
                        'prixProduit' => [
                            'volumeBase' => 120,
                            'montantTtc' => [
                                'libelle' => '',
                                'abonnement' => 11.69,
                                'consommation' => 258.24,
                                'totalTtc' => 269.93,
                            ],
                        ],
                    ],
                ],
                'totaux' => [
                    'libelle' => 'Eau Potable/Assainissement Collectif',
                    'abonnement' => 43.74,
                    'consommation' => 462.53,
                    'totalTtc' => 506.27,
                ],
            ];
        }*/

        $projectDir = $this->getParameter('kernel.project_dir');
        $files = glob($projectDir.'/data/edmc/*-*'.$code_insee.'*RDS.json');
        if ($files && 1 === count($files)) {
            $content = file_get_contents($files[0]);
        } else {
            // Peut être qu'on ne devrait pas retourner HTTP 200 et tableau vide
            // mais je ne sais pas trop comment le client a prévu de gérer ceci
            $content = json_encode([]);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent($content);

        return $response;
    }
}
