<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InterventionTypeRepository")
 */
class InterventionType
{

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="technical_id", type="string", length=32, nullable=false)
     */
    private $technicalId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="advice", type="string", length=255, nullable=false)
     */
    private $advice;

    /**
     * @var string
     *
     * @ORM\Column(name="consequence", type="string", length=255, nullable=false)
     */
    private $consequence;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=false)
     */
    private $icon;

    /**
     * @return string|null
     */
    public function getTechnicalId(): ?string
    {
        return $this->technicalId;
    }

    /**
     * @param string $technicalId
     */
    public function setTechnicalId(string $technicalId): void
    {
        $this->technicalId = $technicalId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getAdvice(): ?string
    {
        return $this->advice;
    }

    /**
     * @param string $advice
     */
    public function setAdvice(string $advice): void
    {
        $this->advice = $advice;
    }

    /**
     * @return string|null
     */
    public function getConsequence(): ?string
    {
        return $this->consequence;
    }

    /**
     * @param string $consequence
     */
    public function setConsequence(string $consequence): void
    {
        $this->consequence = $consequence;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    public function __toString()
    {
        return $this->name . ' (' . $this->technicalId. ')';
    }
}
