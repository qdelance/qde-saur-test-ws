<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParametreQualiteRepository")
 */
class ParametreQualite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Groups({"saur"})
     */
    private $nom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255)
     * @Groups({"saur"})
     */
    private $valeur;

    /**
     * @var string
     *
     * @ORM\Column(name="unite", type="string", length=255)
     * @Groups({"saur"})
     */
    private $unite;

    /**
     * @return mixed
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     */
    public function setCommune($commune): void
    {
        $this->commune = $commune;
    }

    /**
     * @ManyToOne(targetEntity="Commune")
     * @JoinColumn(name="commune_id", referencedColumnName="id")
     */
    private $commune;

    /**
     * ParametreQualite constructor.
     * @param string $nom
     * @param string $valeur
     */
    public function __construct(string $nom = '', string $valeur = '', string $unite = '')
    {
        $this->nom = $nom;
        $this->valeur = $valeur;
        $this->unite = $unite;
    }

    public function __toString()
    {
        return $this->nom . ' ' . $this->valeur . ' ' . $this->unite;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUnite(): string
    {
        return $this->unite;
    }

    /**
     * @param string $unite
     */
    public function setUnite(string $unite): void
    {
        $this->unite = $unite;
    }

    /**
     * Set name
     *
     * @param string $nom
     *
     * @return ParametreQualite
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set value
     *
     * @param string $valeur
     *
     * @return ParametreQualite
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }
}
