<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"saur"})
     */
    private $nom;


    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups({"saur"})
     */
    private $url;


    /**
     * @ORM\Column(type="integer", length=100)
     * @Groups({"saur"})
     */
    private $taille;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"saur"})
     */
    private $type;

    /**
     * @ManyToOne(targetEntity="Commune")
     * @JoinColumn(name="commune_id", referencedColumnName="id")
     */
    private $commune;

    /**
     * Document constructor.
     *
     * @param string $nom    Nom du document
     * @param string $url    URL d'accès au document
     * @param integer $taille Taille en octets
     * @param string $type   type MIME
     */
    public function __construct(string $nom = '', string $url = '', int $taille = 0, string $type = '')
    {
        $this->nom = $nom;
        $this->url = $url;
        $this->taille = $taille;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille): void
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     */
    public function setCommune($commune): void
    {
        $this->commune = $commune;
    }
}
