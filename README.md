qde-saur-test-ws
================

Description
----------------

Fake test WS for Saur tests



Installation
----------------

Requirements : 
* PHP 7.x
* PostgreSQL

Execution: 

```
php -S ip:port -t public
```

DB backend: 

```
docker run -p 5432:5432 --name qde-saur-test-ws -v $(pwd)/docker_postgres_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD=postgres -d postgres
```

then

```
docker start qde-saur-test-ws
```

Gitlab integration
----------------

Gitlab CI is activated, we rely on our own Docker image (as described in Dockerfile).

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/delance/qde-saur-test-ws .
docker push registry.gitlab.com/delance/qde-saur-test-ws
```

Heroku hosting
----------------

With saur-test-ws being the app name:

```
heroku config:set APP_ENV=prod -a saur-test-ws
heroku addons:create heroku-postgresql
heroku config:set DATABASE_URL=postgres://<db_instance_name>@<aws_instance>.eu-west-1.compute.amazonaws.com:5432/<db_name> -a saur-test-ws
```

Then the Procfile is created to ensure Nginx usage, specify Docroot + add Nginx config with rewrite rule.

You also need to set some variables in Heroku admin (in particular for DB access).

WARNING : I've commented the front controller to ensure reading .env file in "prod" environment (APP_ENV), which is NOT
the case by default, and require adding symfony/dotenv in require (instead of require-dev) in composer.json

TODO
----------------

Check if it is possible to handle CORS at the NGinx level, rather than in PHP, to avoid responding to OPTIONS request in SF, in the meantime, I've used cors bundle
Add intervention endpoint (in progress)
Try to enhance API documentation
Be consistent with FR/EN naming of properties (intervention in EN)
Add unit tests
Add human readable labels in EasyAdmin
Add help text close to some fields (like lat/long)
