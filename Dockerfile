FROM php:7.2-fpm-stretch

# https://github.com/debuerreotype/debuerreotype/issues/10
# hack to make postgresql-client install work on slim
RUN mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7

# Install base utilities
# PostgreSQL client is needed for Drush (Drupal)
RUN apt-get update \
 && apt-get install -y \
    libicu-dev libicu57 vim \
    openssl \
    git \
    libpng-dev \
    libjpeg62-turbo-dev libfreetype6-dev libjpeg-dev \
    libmagickwand-dev --no-install-recommends \
    unzip \
    libpq-dev postgresql-client \
 && rm -rf /var/lib/apt/lists/*


# Install PHP core extensions
RUN docker-php-ext-configure pgsql --with-pgsql=/usr/local/pgsql \
    && docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/freetype2/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install pdo pdo_pgsql pgsql gd exif intl opcache soap zip


# Install PHP PECL extensions
RUN pecl install imagick && docker-php-ext-enable imagick

RUN docker-php-ext-enable imagick 

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

# PATH adjustements
# Add global composer binaries to the path (makes drush avalaible, among other things)
ENV PATH="/root/.composer/vendor/bin/:${PATH}"
