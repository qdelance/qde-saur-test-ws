<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APIControllerTest extends WebTestCase
{

    public function testContactSimpleAction()
    {
        $client = static::createClient();
        $client->request('POST', '/api/v1/contact-simple/upload');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAbonnementAction()
    {
        $client = static::createClient();
        $client->request('POST', '/api/v1/abonnement/upload');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testBranchementAction()
    {
        $client = static::createClient();
        $client->request('POST', '/api/v1/branchement/upload');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPingAction()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/ping');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $array = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('code', $array);
        $this->assertArrayHasKey('message', $array);
    }

    public function testContactClienteleAction()
    {
        $client = static::createClient();
        $client->request('POST', '/api/v1/contact/upload');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
}
