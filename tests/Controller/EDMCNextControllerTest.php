<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EMDCNextControllerTest extends WebTestCase
{

    const CODES = [
        '18001',
        '35240',
        '44028',
        '44193',
        '46201',
        '77132',
        '78208',
    ];

    public function testPrix()
    {
        $prefixURL = '/api/prix-eau/';
        $client = static::createClient();

        foreach (self::CODES as $code) {

            $client->request('GET', $prefixURL . $code);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());

            // On teste la structure reçue
            $content = json_decode($response->getContent(), true);
            $this->assertInternalType('array', $content);
            $this->assertCount(3, $content);

            $this->assertArrayHasKey('codeInsee', $content);
            $this->assertArrayHasKey('produits', $content);
            $this->assertArrayHasKey('totaux', $content);

            $produits = $content['produits'];
            foreach ($produits as $produit) {
                $this->assertCount(6, $produit);
                $this->assertArrayHasKey('refCnp', $produit);
                $this->assertArrayHasKey('nom', $produit);
                $this->assertArrayHasKey('type', $produit);
                $this->assertArrayHasKey('sousType', $produit);
                $this->assertArrayHasKey('mission', $produit);
                $this->assertArrayHasKey('prixProduit', $produit);

                // TODO il faudrait continuer
            }
        }
    }

    public function testFrais()
    {
        $prefixURL = '/api/frais/';
        $client = static::createClient();

        foreach (self::CODES as $code) {

            $client->request('GET', $prefixURL . $code);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());

            // On teste la structure reçue
            $content = json_decode($response->getContent(), true);
            $this->assertInternalType('array', $content);
            $this->assertCount(2, $content);

            $this->assertArrayHasKey('codeInsee', $content);
            $this->assertArrayHasKey('produits', $content);

            $produits = $content['produits'];
            foreach ($produits as $produit) {
                $this->assertCount(6, $produit);
                $this->assertArrayHasKey('refCnp', $produit);
                $this->assertArrayHasKey('nom', $produit);
                $this->assertArrayHasKey('type', $produit);
                $this->assertArrayHasKey('sousType', $produit);
                $this->assertArrayHasKey('mission', $produit);
                $this->assertArrayHasKey('frais', $produit);

                // TODO il faudrait continuer
            }
        }
    }

    public function testRDS()
    {
        $prefixURL = '/api/rds/';
        $client = static::createClient();

        foreach (self::CODES as $code) {

            $client->request('GET', $prefixURL . $code);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());

            // On teste la structure reçue
            $content = json_decode($response->getContent(), true);
            $this->assertInternalType('array', $content);
            $this->assertCount(2, $content);

            $this->assertArrayHasKey('codeInsee', $content);
            $this->assertArrayHasKey('rds', $content);

            $rdss = $content['rds'];
            foreach ($rdss as $rds) {
                $this->assertCount(7, $rds);
                $this->assertArrayHasKey('refCnp', $rds);
                $this->assertArrayHasKey('type', $rds);
                $this->assertArrayHasKey('sousType', $rds);
                $this->assertArrayHasKey('libelle', $rds);
                $this->assertArrayHasKey('taille', $rds);
                $this->assertArrayHasKey('typeMime', $rds);
                $this->assertArrayHasKey('url', $rds);
            }
        }
    }
}
